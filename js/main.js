$(function() {
    // Collapse Advanced Button
    $('#collapseAdvanced').on('shown.bs.collapse', function () {                        
        $('#advancedButton').html('Back to Basic');
    });  
    
    $('#collapseAdvanced').on('hidden.bs.collapse', function () {                        
        $('#advancedButton').html('Advanced');
    });       

    // Add fields in Advanced Block            
    
    var maxFields = 5; 
    
    // Names           
    var nameHTML = '<div class="row"><label class="col-md-2"></label><div class="col-md-7"><div class="form-row"><div class="form-group col-md-4"><input type="text" name="firstname[]" class="form-control" placeholder="First Name"></div><div class="form-group col-md-4"><input type="text" name="middlename[]" class="form-control" placeholder="Middle Name"></div><div class="form-group col-md-4"><input type="text" name="lastname[]" class="form-control" placeholder="Last Name"></div></div></div><div class="col-md-3"><a href="javascript:void(0);" class="remove text-sm text-danger"><i data-feather="x" class="f-sm"></i> Remove</a></div></div>';
    var names = 1; 
    $('#addName').click(function () {            
        if(names < maxFields) {                     
            $('#advNames').append(nameHTML);
            names++;
        }
    });
    $('#advNames').on('click', '.remove', function (e) {
        e.preventDefault();
        $(this).parent('div').parent('div').remove();
        names--; 
    });

    // Emails
    var emailHTML = '<div class="row"><label class="col-md-2"></label><div class="col-md-7"><div class="form-row"><div class="form-group col-md-12"><input type="email" name="email[]" class="form-control" placeholder="Email"></div></div></div><div class="col-md-3"><a href="javascript:void(0);" class="remove text-sm text-danger"><i data-feather="x" class="f-sm"></i> Remove</a></div></div>';             
    var emails = 1; 
    $('#addEmail').click(function () {            
        if(emails < maxFields) {                     
            $('#advEmails').append(emailHTML);
            emails++;
        }
    });
    $('#advEmails').on('click', '.remove', function (e) {
        e.preventDefault();
        $(this).parent('div').parent('div').remove();
        emails--; 
    });

    // Phones
    var phoneHTML = '<div class="row"><label class="col-md-2"></label><div class="col-md-7"><div class="form-row"><div class="form-group col-md-4"><input type="text" name="country-code[]" class="form-control" placeholder="Country code"></div><div class="form-group col-md-8"><input type="text" name="phone[]" class="form-control" placeholder="Phone"></div></div></div><div class="col-md-3"><a href="javascript:void(0);" class="remove text-sm text-danger"><i data-feather="x" class="f-sm"></i> Remove</a></div></div>';             
    var phones = 1; 
    $('#addPhone').click(function () {            
        if(phones < maxFields) {                     
            $('#advPhones').append(phoneHTML);
            phones++;
        }
    });
    $('#advPhones').on('click', '.remove', function (e) {
        e.preventDefault();
        $(this).parent('div').parent('div').remove();
        phones--; 
    });

    // Username
    var usernameHTML = '<div class="row"><label class="col-md-2"></label><div class="col-md-7"><div class="form-row"><div class="form-group col-md-12"><input type="text" name="username[]" class="form-control" placeholder="Username"></div></div></div><div class="col-md-3"><a href="javascript:void(0);" class="remove text-sm text-danger"><i data-feather="x" class="f-sm"></i> Remove</a></div></div>';             
    var usernames = 1; 
    $('#addUsername').click(function () {            
        if(usernames < maxFields) {                     
            $('#advUsernames').append(usernameHTML);
            usernames++;
        }
    });
    $('#advUsernames').on('click', '.remove', function (e) {
        e.preventDefault();
        $(this).parent('div').parent('div').remove();
        usernames--; 
    });

    // Addr
    var addrHTML = '<div class="row"><label class="col-md-2"></label><div class="col-md-7"><div class="form-row"><div class="form-group col-md-2"><input type="text" name="house[]" class="form-control" placeholder="House"></div><div class="form-group col-md-3"><input type="text" name="street[]" class="form-control" placeholder="Street"></div><div class="form-group col-md-2"><input type="text" name="zip[]" class="form-control" placeholder="Zip"></div><div class="form-group col-md-5"><input type="text" name="addr-global[]" class="form-control" placeholder="City, State an Country"></div></div></div><div class="col-md-3"><a href="javascript:void(0);" class="remove text-sm text-danger"><i data-feather="x" class="f-sm"></i> Remove</a></div></div>';             
    var addrs = 1; 
    $('#addAddr').click(function () {            
        if(addrs < maxFields) {                     
            $('#advAddrs').append(addrHTML);
            addrs++;
        }
    });
    $('#advAddrs').on('click', '.remove', function (e) {
        e.preventDefault();
        $(this).parent('div').parent('div').remove();
        addrs--; 
    });

    // Assoc
    var assocHTML = '<div class="row"><label class="col-md-2"></label><div class="col-md-7"><div class="form-row"><div class="form-group col-md-4"><input type="text" name="assoc-firstname[]" class="form-control" placeholder="First Name"></div><div class="form-group col-md-4"><input type="text" name="assoc-middlename[]" class="form-control" placeholder="Middle Name"></div><div class="form-group col-md-4"><input type="text" name="assoc-lastname[]" class="form-control" placeholder="Last Name"></div></div></div><div class="col-md-3"><a href="javascript:void(0);" class="remove text-sm text-danger"><i data-feather="x" class="f-sm"></i> Remove</a></div></div>';             
    var assocs = 1; 
    $('#addAssocName').click(function () {            
        if(assocs < maxFields) {                     
            $('#advAssocNames').append(assocHTML);
            assocs++;
        }
    });
    $('#advAssocNames').on('click', '.remove', function (e) {
        e.preventDefault();
        $(this).parent('div').parent('div').remove();
        assocs--; 
    });

    // Edu
    var eduHTML = '<div class="row"><label class="col-md-2"></label><div class="col-md-7"><div class="form-row"><div class="form-group col-md-6"><input type="text" name="school[]" class="form-control" placeholder="School"></div><div class="form-group col-md-6"><input type="text" name="academic[]" class="form-control" placeholder="Academic Degree"></div></div></div><div class="col-md-3"><a href="javascript:void(0);" class="remove text-sm text-danger"><i data-feather="x" class="f-sm"></i> Remove</a></div></div>';             
    var edu = 1; 
    $('#addEdu').click(function () {            
        if(edu < maxFields) {                     
            $('#advEdu').append(eduHTML);
            edu++;
        }
    });
    $('#advEdu').on('click', '.remove', function (e) {
        e.preventDefault();
        $(this).parent('div').parent('div').remove();
        edu--; 
    });

    // Jobs
    var jobHTML = '<div class="row"><label class="col-md-2 font-weight-bold">Jobs</label><div class="col-md-7"><div class="form-row"><div class="form-group col-md-6"><input type="text" name="organization[]" class="form-control" placeholder="Organization"></div><div class="form-group col-md-6"><input type="text" name="job-title[]" class="form-control" placeholder="Job Title"></div></div></div><div class="col-md-3"><a href="javascript:void(0);" class="remove text-sm text-danger"><i data-feather="x" class="f-sm"></i> Remove</a></div></div>';             
    var jobs = 1; 
    $('#addJob').click(function () {            
        if(jobs < maxFields) {                     
            $('#advJobs').append(jobHTML);
            jobs++;
        }
    });
    $('#advJobs').on('click', '.remove', function (e) {
        e.preventDefault();
        $(this).parent('div').parent('div').remove();
        jobs--; 
    });
});